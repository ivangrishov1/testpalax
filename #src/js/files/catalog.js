class Products {
    constructor(url) {
        this.url = url;
        this.CATALOG = [];
    }
    filterSlider() {
        let input1 = inputs[0].value
        let input2 = inputs[1].value

        let filters_array = this.CATALOG.filter(function(item) {
            if (item.minPrice >= input1 && item.minPrice <= input2) {
                return true
            }
        });

        this.CATALOG = filters_array
    }
    render() {
        let htmlCatalog = '';

        this.CATALOG.forEach(({id, name, housingComplexName, planLink, photos, sameLayoutFlatCount, minPrice, numberOfRooms}) => {

            htmlCatalog += `<a href="#" class="catalog__card card data-price=${minPrice}">
                        <div class="card__inner">
                            <div class="card__img-wrapper">
                                <img src="${planLink}" alt="">
                            </div>
                            <div class="card__text-wrapper">
                                <div class="card__title-wrapper">
                                    <h4 class="card__title">${name}</h4>
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.798 7.45201L11.5129 8.15659L12.2148 7.43912L13.341 6.28799C15.0921 4.56865 17.9446 4.57067 19.6931 6.29405C21.4356 8.01152 21.4356 10.803 19.6931 12.5204L11.4983 20.5975L3.30687 12.5618C3.30665 12.5616 3.30643 12.5613 3.30621 12.5611C1.56438 10.8436 1.5646 8.05266 3.30687 6.33541C5.05741 4.61 7.91462 4.61 9.66516 6.33541L10.3671 5.6232L9.66516 6.33541L10.798 7.45201Z" stroke="#D6DCE1" stroke-width="2"/>
                                    </svg>
                                </div>
                                <h5 class="card__housing">${housingComplexName}</h5>
                                <div class="card__numbers-wrap">
                                    <div class="card__number-room">${sameLayoutFlatCount} квартир</div>
                                    <div class="card__number-price">от ${Number(minPrice[0]) + ',' + Number(minPrice[1])} млн ₽</div>
                                </div>
                                <div class="card__button-wrapper">
                                    <button class="card__button">выбери свою</button>
                                </div>
                            </div>
                        </div>
                    </a>`
        })

        let catalog_block = document.querySelector('.catalog__cards-wrapper');

        catalog_block.innerHTML = htmlCatalog;
    }
    async getResourse() {

        const response = await fetch(this.url)
            .then(res => res.json())
            .then(body => {
                this.CATALOG = body;

                this.filterSlider()

                render()
            })
            .catch(error => {
                console.log(error);
            })
    }
}

const productsPage = new Products('test-server/catalog.json'); /// тестовые данные - "test-server/catalog.json" ; реальный api - "http://myjson.dit.upm.es/api/bins/c3dd"

function render() {
    productsPage.render()
}

productsPage.getResourse()
