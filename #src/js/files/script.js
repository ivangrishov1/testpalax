document.addEventListener("DOMContentLoaded", function(event) {

    //// toggle column

    let icons = document.querySelectorAll('.header-bottom__net-item');
    let catalog_wrapper = document.querySelector('.catalog__cards-wrapper');

    icons.forEach(item => {
        item.addEventListener('click', function () {
            icons.forEach(item => {
                item.classList.remove('header-bottom__net-item--active')
            })
            item.classList.add('header-bottom__net-item--active')

            let svg_icon = item.querySelector('svg');

            if (svg_icon.classList.contains('header-bottom__net-item-list')) {
                catalog_wrapper.classList.remove('catalog__cards--col')
                catalog_wrapper.classList.add('catalog__cards--line')
            } else {
                catalog_wrapper.classList.remove('catalog__cards--line')
                catalog_wrapper.classList.add('catalog__cards--col')
            }
        })
    })

    //// custom checkbox

    let filters_items = document.querySelectorAll('.filters__list-item');
    filters_items.forEach(item => {
        item.addEventListener('click', function () {
            item.classList.toggle('filters__list-item--active')
        })
    })


    ///// favorite icon

    let favorite_icons = document.querySelectorAll('.card__title-wrapper svg');

    favorite_icons.forEach(item => {
        item.addEventListener('click', function (event) {
            event.preventDefault();
            item.closest('.card').classList.toggle('card--favorite')
        });
    })

});
